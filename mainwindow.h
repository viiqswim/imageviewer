#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QtXml/QXmlStreamReader>
#include <album.h>
#include <photo.h>


class QScrollArea;
class QLabel;

namespace Ui {
class MainWindow;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class is the central execution point for everything
 * that happens in the program
 *
 *****************************************************************************/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    // Checks whether album is empty
    bool isAlbumEmpty();
    // Returns a reference to the album
    Album getAlbum();
    // Gets the index of the current image being viewed
    int getCurrentImageIndex();
    // Gets the image label for the photo viewer
    QLabel *getImageLabel();
    // Sets the date of the photo
    void setPhotoDate(QString date);
    // Sets the location of the photo
    void setPhotoLocation(QString date);
    // Sets the description of the photo
    void setPhotoDescription(QString date);
    // Sets a new photo
    void setNewPhoto(QPixmap pixMap, QRect qRect);
    // Saves the modified image to the file path owned by an image
    bool saveImageInFile(QPixmap pixMap);
    // Brightens an image
    void brightenImage(QString fileName, int level);
    // Contrasts an image
    void contrastImage(QString fileName, int level);
    // Finds edges in an image
    void edgeImage(QString fileName);
    // Modified gamma in an image
    void gammaImage(QString fileName, int level);
    // Negates an image
    void negateImage(QString fileName);
    // Sharpens an image
    void sharpenImage(QString fileName, int level);
    // Smooths an image
    void smoothImage(QString fileName, float level);
    // Resizes an image
    void resizeImage(QString fileName, int percentage);
    // Rotates an image
    void rotateImage(QString fileName, int angle);
    // Opens a photo for a given index in the album of photos
    void openPhoto(int index);
    ~MainWindow();

// All the slots - names are self-explanatory
private slots:
    void open();
    void about();
    void usage();
    void pageForward();
    void pageBackward();
    void moveForward();
    void moveBackward();
    void closeAlbum();
    void newAlbum();
    bool addPhoto();
    void deletePhoto();
    void saveAlbum();
    void saveAsAlbum();
    void editDescription();
    void cropImage();
    void brighten();
    void contrast();
    void edge();
    void gamma();
    void negate();
    void sharpen();
    void smooth();
    void resize();
    void rotate();

private:
    Ui::MainWindow *ui;
    // Connects signals to slots, and creates tooltips
    void createActions();
    // Opens an XML file
    bool openXMLFile();
    // Reads XML file that was opened
    void readXMLFile(QXmlStreamReader& xml);
    // Adds XML photos from XML file to the Album object
    void addPhotosFromXMLToAlbum(QXmlStreamReader& xml);
    // Creates a generic dialog box with an arbitrary title and message
    void createDialogBox(QString title, QString message);
    // Saves an image to its file path
    void _save(bool saveAsFlag);

    // Contains a vector of photos
    Album *album;
    // Contains filename, description, location, and date
    Photo *photo;
    // Open file action
    QAction *openAct;
    // Image container
    QLabel *imageLabel;
    // Image label container
    QScrollArea *scrollArea;
    // Holds current image index being viewed
    int current_image_index;
    // Holds the current name of the album, and where to save it
    QString albumFileName;
    // Holds cropping image label
    QLabel *croppingImageLabel;
};

#endif // MAINWINDOW_H
