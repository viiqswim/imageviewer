#ifndef CROPPINGIMAGELABEL_H
#define CROPPINGIMAGELABEL_H

#include <QtGui>
#include "mainwindow.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class spawns a new window where an image to
 * be cropped. It handles everything from keeping track of
 * where the user wants to crop, to saving the new cropped image to a file
 *
 *****************************************************************************/
//
class CroppingImageLabel : public QLabel
{
public:
    CroppingImageLabel();

public:
  CroppingImageLabel( QString fileName, MainWindow *_mainWindow );
  // Called when a user presses a mouse button on cropping image label
  void mousePressEvent( QMouseEvent *event );
  // Called when a user moves a mouse on cropping image label
  void mouseMoveEvent( QMouseEvent *event );
  // Called when a user releases mouse on cropping image label
  void mouseReleaseEvent( QMouseEvent *event );
  // Called when a user presses a key on cropping image label
  void keyPressEvent( QKeyEvent *event );

private:
  // Rubberband where the cropping will happen
  QRubberBand *rubberBand;
  // Point where rubberband will start
  QPoint origin;
  // Pointer back to program's main functions
  MainWindow *mainWindow;
};

#endif // CROPPINGIMAGELABEL_H
