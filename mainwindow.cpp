#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>

#include <QtCore>
#include <QtXml>
#include "editdescription.h"
#include "croppingimagelabel.h"
#include "sliderbardialog.h"
#include "spinnerboxdialog.h"
#include <QStatusBar>

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Constructor
 *
 *
 * @param[in] parent - Container widget
 *
 * @returns nothing
 *****************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Photo Album");

    // Sets class imagelabel to the widget's image label
    imageLabel = ui->imageLabel;
    // Sets background role
    imageLabel->setBackgroundRole(QPalette::Base);
    // Ignores size policy
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    // Sets images border
    imageLabel->setStyleSheet("border-style: solid; border-width: 10px; border-radius: 7px;");

    // Sets it so that image label grows with the scroll bar
    imageLabel->setScaledContents(true);

    // Sets class scrollarea to the widget's scroll area
    scrollArea = ui->scrollArea;
    // Sets background role
    scrollArea->setBackgroundRole(QPalette::Dark);

    // Creates new Album and Photo
    album = new Album();
    photo = new Photo();

    // Starts image index at 0
    current_image_index = 0;

    // Creates a new album
    newAlbum();
    // Connects signals to slots, and buttons to tooltips
    createActions();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Deconstructor
 *
 * @returns nothing
 *****************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
    delete album;
    delete photo;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Gets the current album being used
 *
 * @returns Album
 *****************************************************************************/
Album MainWindow::getAlbum(){
    return *album;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Gets the index of the current image being viewed
 *
 * @returns int
 *****************************************************************************/
int MainWindow::getCurrentImageIndex(){
    return current_image_index;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Gets the main window's image label
 *
 * @returns QLabel
 *****************************************************************************/
QLabel *MainWindow::getImageLabel(){
    return imageLabel;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Sets the photo date
 *
 *
 * @param[in] date - String containing date
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::setPhotoDate(QString date){
    album->photos[current_image_index].date = (date);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Sets the photo description
 *
 *
 * @param[in] date - String containing description
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::setPhotoDescription(QString description){
    album->photos[current_image_index].description= description;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Sets the photo location
 *
 *
 * @param[in] date - String containing location
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::setPhotoLocation(QString location){
    album->photos[current_image_index].location = location;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Brightens an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::brightenImage(QString fileName, int level){
    QImage image(fileName);

    // brighten the image by 64 intensity levels (25% of intensity range)
    for ( int x = 0; x < image.width(); x++ )
    {
        for ( int y = 0; y < image.height(); y++ )
        {
            QRgb p = image.pixel( x, y );
            int r = qRed( p ) + level;
            if ( r > 255 ) r = 255;
            int g = qGreen( p ) + level;
            if ( g > 255 ) g = 255;
            int b = qBlue( p ) + level;
            if ( b > 255 ) b = 255;
            image.setPixel( x, y, qRgb( r, g, b ) );
        }
    }
    QPixmap newPixmap = QPixmap::fromImage(image);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(image.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Contrasts an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::contrastImage(QString fileName, int level){
    QImage image(fileName);

    // contrast stretch image between intensity levels 64 and 192
    for ( int x = 0; x < image.width(); x++ )
    {
        for ( int y = 0; y < image.height(); y++ )
        {
            QRgb p = image.pixel( x, y );
            int r = ( qRed( p ) - level ) * 2;
            if ( r < 0 ) r = 0; else if ( r > 255 ) r = 255;
            int g = ( qGreen( p ) - level ) * 2;
            if ( g < 0 ) g = 0; else if ( g > 255 ) g = 255;
            int b = ( qBlue( p ) - level ) * 2;
            if ( b < 0 ) b = 0; else if ( b > 255 ) b = 255;
            image.setPixel( x, y, qRgb( r, g, b ) );
        }
    }

    QPixmap newPixmap = QPixmap::fromImage(image);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(image.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Makes edges of an image more noticeable
 *
 * @param[in] fileName - file path of image
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::edgeImage(QString fileName){
    QImage image(fileName);

    // find edges
    QImage edgeImage( image.width(), image.height(), QImage::Format_RGB32 );
    for ( int x = 1; x < image.width() - 1; x++ )
    {
        for ( int y = 1; y < image.height() - 1; y++ )
        {
            // pseudo-Prewitt edge magnitude
            int Gx = qGray( image.pixel( x, y + 1 ) ) - qGray( image.pixel( x, y - 1 ) );
            int Gy = qGray( image.pixel( x + 1, y ) ) - qGray( image.pixel( x - 1, y ) );
            int e = 3 * sqrt( Gx * Gx + Gy * Gy );
            if ( e > 255 ) e = 255;
            edgeImage.setPixel( x, y, qRgb( e, e, e ) );
        }
    }

    QPixmap newPixmap = QPixmap::fromImage(edgeImage);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(edgeImage.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Modifies gamma on an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::gammaImage(QString fileName, int level){
    QImage image(fileName);

    // alter image gamma
    double gamma = level;
    for ( int x = 0; x < image.width(); x++ )
    {
        for ( int y = 0; y < image.height(); y++ )
        {
            QRgb p = image.pixel( x, y );
            int r = pow( qRed( p ) / 255.0, gamma ) * 255 + 0.5;
            int g = pow( qGreen( p ) / 255.0, gamma ) * 255 + 0.5;
            int b = pow( qBlue( p ) / 255.0, gamma ) * 255 + 0.5;
            image.setPixel( x, y, qRgb( r, g, b ) );
        }
    }

    QPixmap newPixmap = QPixmap::fromImage(image);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(image.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Negates an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::negateImage(QString fileName){
    QImage image(fileName);

    // negate the image prior to display
    image.invertPixels();

    QPixmap newPixmap = QPixmap::fromImage(image);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(image.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Sharpens an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::sharpenImage(QString fileName, int level){
    QImage image(fileName);

    // sharpen the image prior to display, using
    //  0 -1  0
    // -1  5 -1
    //  0 -1  0
    QImage sharpImage( image );
    for ( int x = 1; x < image.width() - 1; x++ )
    {
        for ( int y = 1; y < image.height() - 1; y++ )
        {
            QRgb p = image.pixel( x, y );
            int r = level * qRed( p );
            int g = level * qGreen( p );
            int b = level * qBlue( p );
            p = image.pixel( x - 1, y );
            r -= qRed( p );
            g -= qGreen( p );
            b -= qBlue( p );
            p = image.pixel( x + 1, y );
            r -= qRed( p );
            g -= qGreen( p );
            b -= qBlue( p );
            p = image.pixel( x, y - 1 );
            r -= qRed( p );
            g -= qGreen( p );
            b -= qBlue( p );
            p = image.pixel( x, y + 1 );
            r -= qRed( p );
            g -= qGreen( p );
            b -= qBlue( p );

            if ( r < 0 ) r = 0; else if ( r > 255 ) r = 255;
            if ( g < 0 ) g = 0; else if ( g > 255 ) g = 255;
            if ( b < 0 ) b = 0; else if ( b > 255 ) b = 255;

            sharpImage.setPixel( x, y, qRgb( r, g, b ) );
        }
    }

    QPixmap newPixmap = QPixmap::fromImage(sharpImage);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(sharpImage.size());
        imageLabel->setPixmap( newPixmap );
    }


}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Smooths an image
 *
 * @param[in] fileName - file path of image
 * @param[in] level - how much to perform operation
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::smoothImage(QString fileName, float level){
    QImage image(fileName);

    // smooth the image prior to display
    QImage smoothImage( image );
    for ( int x = 1; x < image.width() - 1; x++ )
    {
        for ( int y = 1; y < image.height() - 1; y++ )
        {
            int r = 0, g = 0, b = 0;
            for ( int m = -1; m <= 1; m++ )
            {
                for ( int n = -1; n <= 1; n++ )
                {
                    QRgb p = image.pixel( x + m, y + n );
                    r += qRed( p );
                    g += qGreen( p );
                    b += qBlue( p );
                }
            }
            smoothImage.setPixel( x, y, qRgb( r / level, g / level, b / level ) );
        }
    }

    QPixmap newPixmap = QPixmap::fromImage(smoothImage);

    if(saveImageInFile(newPixmap)){
        imageLabel->setFixedSize(smoothImage.size());
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Rotates an image
 *
 * @param[in] fileName - file path of image
 * @param[in] angle - degree to rotate image
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::rotateImage(QString fileName, int angle){
    // load image from file into pixmap
    QPixmap pix( fileName );

    // create a Qtransform for rotation (can also use for translation and scaling)
    QTransform *t = new QTransform;

    // Transforms pixmap
    QPixmap newPixmap = pix.transformed( t->rotate( angle ) );

    if(saveImageInFile(newPixmap)){
        // more concisely, can collapse the previous three lines into one:
        imageLabel->setPixmap( newPixmap );
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Modifies size of an image
 *
 * @param[in] fileName - file path of image
 * @param[in] percentage - resize percentage
 *
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::resizeImage(QString fileName, int percentage){
    // load image from file into pixmap
    QPixmap pix( fileName );

    float percent = percentage / 100.0;

    // Gets current image width and height
    float w = pix.width(), h = pix.height();
    // Modified width and height with given percentage
    w *= percent, h *= percent;

    // Sets pixmap to scaled image
    QPixmap newPixmap = pix.scaled( w, h, Qt::KeepAspectRatio );

    // Saves image
    if(saveImageInFile(newPixmap)){
        openPhoto(current_image_index);
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Saves image to the filepath specified in XML file
 *
 * @param[in] pixmap - what to output to filepath
 *
 * @returns bool - true if user wanted to save, false if user didn't want to save
 *****************************************************************************/
bool MainWindow::saveImageInFile(QPixmap pixMap){
    // Initializes msgbox and message
    QString message = "Doing this will modify the contents of the image stored in your computer";
    QMessageBox msgBox;

    // Sets msgbox text to the message specified
    msgBox.setText(message);
    //Sets msgbox text
    msgBox.setInformativeText("Are you sure you want to perform this action?");
    // Sets buttons
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
    // Sets default button
    msgBox.setDefaultButton(QMessageBox::Cancel);
    // Getsthe user's choice
    int choice = msgBox.exec();

    switch (choice) {
    // If user chose to save, save image pixmap to filepath and return true
    case QMessageBox::Save: {
        QImage image = pixMap.toImage();
        image.save(album->photos.at(current_image_index).filename);
        return true;
    }
        // Otherwise return false
    default:
        return false;
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Opens XML file
 *
 * @returns bool - true if file successfully opened, false if it didn't
 *****************************************************************************/
bool MainWindow::openXMLFile(){
    // Opens file dialog, and asks user for a filepath
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), QDir::currentPath());
    // If filename is empty, we couldnt open file
    if(fileName.isEmpty()){
        return false;
    }
    // Creates a new album
    newAlbum();
    // Sets current image being viewed to 0
    current_image_index = 0;
    // Sets album file name to the file name specified by user
    albumFileName = fileName;
    // Opens file in filename
    QFile* file = new QFile(fileName);

    // If can't open file, throw error
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,
                              "QXSRExample::parseXML",
                              "Couldn't open example.xml",
                              QMessageBox::Ok);
        return false;
    }

    // Puts file in an XML reader so its easier to read
    QXmlStreamReader xml(file);
    // Reads XML file
    readXMLFile(xml);
    // Deletes file descriptor
    delete file;

    return true;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Reads XML file
 *
 * @param[in] xml - contains xml to parse
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::readXMLFile(QXmlStreamReader& xml){
    // We'll parse the XML until we reach end of it.
    while(!xml.atEnd() &&  !xml.hasError()) {
        // Read next element
        QXmlStreamReader::TokenType token = xml.readNext();
        // If token is just StartDocument, we'll go to next.
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        // If token is StartElement, we'll see if we can read it.
        if(token == QXmlStreamReader::StartElement) {
            if(xml.name() == "photo"){
                xml.readNext();
                addPhotosFromXMLToAlbum(xml);
            }
        }
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Adds photos from XML to the Album class
 *
 * @param[in] xml - contains xml to parse
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::addPhotosFromXMLToAlbum(QXmlStreamReader& xml){
    // Traverse the photo's description items
    while(!(xml.tokenType() == QXmlStreamReader::EndElement)) {
        // Gets the name of dom element being parsed
        QStringRef name = xml.name();
        // Gets the dom element's text
        QString elementText = xml.readElementText();

        // Set photo attribute depending on the name of the dom element
        if(name == "filename") {
            photo->filename = elementText;
        } else if(name == "date") {
            photo->date = elementText;
        } else if(name == "location") {
            photo->location = elementText;
        } else if(name == "description") {
            photo->description = elementText;
        }

        // Read next xml element
        xml.readNext();
    }

    // Push back new photo to the album of photos
    album->photos.push_back(*photo);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Opens a photo for a given index in the photo album
 *
 * @param[in] index - index to open from photo album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::openPhoto(int index){
    // If photo is empty, return error message
    if(album->photos.empty()){
        createDialogBox("Error", "File is empty.");
        return;
    }
    // Get filepath of current image
    QString filePath = album->photos.at(index).filename;
    // Set default values for date, location, and description
    QString date = "Date: ";
    QString location = "Location: ";
    QString description = "Description: ";

    // If there is content in date, location, and description, then set them
    // Otherwise, say they are undefined
    if(album->photos.at(index).date != ""){
        date += album->photos.at(index).date;
    } else {
        date += "Undefined";
    }
    if(album->photos.at(index).location != ""){
        location += album->photos.at(index).location;
    } else {
        location += "Undefined";
    }
    if(album->photos.at(index).description != ""){
        description += album->photos.at(index).description;
    } else {
        description += "Undefined";
    }

    // Get image in filepath within a QImage
    QImage image(filePath);

    // If the image being processed is null, then there was an error with it
    if (image.isNull()) {
        createDialogBox("Error", "Cannot load a photo in the album");
        return;
    }

    // Clears current image
    imageLabel->clear();
    // Sets new image from filepath
    imageLabel->setPixmap(QPixmap::fromImage(image));
    // Set fixed size
    imageLabel->setFixedSize(image.size());

    // Set labels to pertinent text
    ui->label->setText(QString::number(current_image_index + 1) + " / " + QString::number(album->photos.size()));
    ui->dateLabel->setText(date);
    ui->locationLabel->setText(location);
    ui->descriptionLabel->setText(description);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Opens a photo
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::open()
{
    // Check if we could open XML file
    if(openXMLFile()) {
        openPhoto(current_image_index);
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Views next image in the album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::pageForward()
{
    if(album->photos.size() <= 1)
    {
        return;
    }

    if(album->photos.size() <= current_image_index + 1)
    {
        current_image_index = -1;
    }
    openPhoto(++current_image_index);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Views last image in the album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::pageBackward()
{
    if(album->photos.size() <= 1)
    {
        return;
    }

    if(current_image_index - 1 < 0)
    {
        current_image_index = album->photos.size();
    }
    openPhoto(--current_image_index);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Swaps image with the image in front of it in the album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::moveForward()
{
    // If we are trying to swap with a nonexistent image, do nothing
    if(current_image_index == album->photos.size() - 1 || album->photos.size() == 0){
        return;
    }
    // Swap images
    Photo temp = album->photos.at(current_image_index);
    album->photos[current_image_index] = album->photos[current_image_index + 1];
    album->photos[current_image_index + 1] = temp;

    pageForward();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Swaps image with the image behind in the album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::moveBackward()
{
    // If we are trying to swap with a nonexistent image, do nothing
    if(current_image_index == 0){
        return;
    }
    // Swap images
    Photo temp = album->photos.at(current_image_index);
    album->photos[current_image_index] = album->photos[current_image_index - 1];
    album->photos[current_image_index - 1] = temp;

    pageBackward();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Closes current album being viewed
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::closeAlbum()
{
    albumFileName = "";
    // If photo album is empty, do nothing
    if(album->photos.isEmpty()){
        return;
    }
    // Clear Vector within Photo Album
    album->photos.clear();
    // Clear Image Label
    imageLabel->clear();
    current_image_index = 0;
    // Set labels to pertinent text

    ui->label->setText("");
    ui->dateLabel->setText("Date: No photo to display");
    ui->locationLabel->setText("Location: No photo to display");
    ui->descriptionLabel->setText("Description: No photo to display");
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Sets the environment up for a new album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::newAlbum(){
    closeAlbum();
    // Clear Image Label
    imageLabel->clear();
    imageLabel->setText("Click 'Add Photo' to add photos");
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Adds a photo to the album
 *
 * @returns bool - false if couldn't open file, true if it could
 *****************************************************************************/
bool MainWindow::addPhoto(){
    // Open the file dialog, and ask user to get photo path
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), QDir::currentPath());
    // If file name is empty, there was an error
    if(fileName.isEmpty()){
        return false;
    }

    // Creates a new photo
    Photo *temp = new Photo();
    // sets photo's file name
    temp->filename = fileName;

    // Pushes new photo to album of photos
    album->photos.push_back(*temp);

    // Opens the photo
    openPhoto(album->photos.size() - 1);
    // Sets the viewer to the image we just opened
    current_image_index = album->photos.size() - 1;

    // Asks user to enter a description for the new image
    editDescription();

    // deletes temporary photo
    delete temp;

    return true;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Deletes the current photo being viewed from album
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::deletePhoto(){
    // If album is empty, throw error and return
    if(album->photos.isEmpty()){
        createDialogBox("No more photos", "There are no more photos to delete.");
        return;
    }

    // If there is only one image left, set the current index to 0
    if(album->photos.size() == 1){
        // Clear Image Label
        imageLabel->clear();
        imageLabel->setText("Click 'Add Photo' to add photos");
        // Set labels to pertinent text
        ui->label->setText("");
        ui->dateLabel->setText("Date: No photo to display");
        ui->locationLabel->setText("Location: No photo to display");
        ui->descriptionLabel->setText("Description: No photo to display");
    } else {
        // Delete photo user is currently looking at
        album->photos.remove(current_image_index);
        // Move to the next photo
        if(current_image_index - 1 == album->photos.size() - 1){
            current_image_index--;
        }
        // Open another photo
        openPhoto(current_image_index);
    }

}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Saves album in filepath where xml file came from
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::saveAlbum(){
    _save(false);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Saves album with new filepath
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::saveAsAlbum(){
    _save(true);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Saves album
 *
 * @param[in] saveAsFlag - tells whether the user wants to do 'save' or 'save as'
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::_save(bool saveAsFlag){
    if(albumFileName.isEmpty() || saveAsFlag){
        // Open the file dialog, and ask user to get photo path
        albumFileName = QFileDialog::getOpenFileName(this,
                                                        tr("Save As"), QDir::currentPath());
    }
    // is album file name is empty, there was an error, so return and do nothing
    if(albumFileName.isEmpty()){
        return;
    }

    // Create a DOM element
    QDomDocument document;
    // Create root element
    QDomElement root = document.createElement("album");
    // Add root element to the DOM
    document.appendChild(root);
    for(int i = 0; i < album->photos.size(); i++){
        // Create a photo
        QDomElement photo = document.createElement("photo");

        // Add elements to the photo
        QDomElement filename = document.createElement("filename");
        QDomText text = document.createTextNode(album->photos[i].filename);
        filename.appendChild(text);
        photo.appendChild(filename);

        QDomElement date = document.createElement("date");
        text = document.createTextNode(album->photos[i].date);
        date.appendChild(text);
        photo.appendChild(date);

        QDomElement location = document.createElement("location");
        text = document.createTextNode(album->photos[i].location);
        location.appendChild(text);
        photo.appendChild(location);

        QDomElement description = document.createElement("description");
        text = document.createTextNode(album->photos[i].description);
        description.appendChild(text);
        photo.appendChild(description);

        // Add it to the root element
        root.appendChild(photo);
    }

    // Try to open the file
    QFile file(albumFileName);

    // If file didn't open, t hor error
    if(!file.open(QIODevice::WriteOnly|QIODevice::Text)) {
        createDialogBox("Information", "There was an error trying to save your file. Try again tomorrow.");
        return;
    }
    else {
        // Otherwise, output the XML element we just created
        QTextStream stream(&file);
        stream << document.toString();
        // close file
        file.close();
        // tell the user that file has been saved
        createDialogBox("Information", "Your file has been saved");
    }

}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Display about message
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::about()
{
    QString aboutMsg = "This is version 1.0 of the ImageViewer created in the Graphical User Interfaces class.";
    aboutMsg += "\nThe authors of this program are Victor Dozal and Subhakar Yarlagadda.";
    aboutMsg += "\nThis software was created using Qt4, which runs on C++.";
    createDialogBox("About", aboutMsg);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Display usage message
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::usage()
{
    QString usageMsg = "This program is separated into 4 sections.";
    usageMsg += "\nAt the very top, you will find four menus containing all possible operations: ";
    usageMsg += "File, Edit, Image, and Help";
    usageMsg += "\nBelow the menus, you will find a toolbar where you can access common functionality in an easier way.";
    usageMsg += "\nIn the middle section, photos from an album will be displayed. ";
    usageMsg += "\nAt the bottom, there will be information for the image, including the following:";
    usageMsg += "\nWhat image in the album you are looking at, location where image was taken, date image was taken, ";
    usageMsg += "and a brief description of the photo.";
    createDialogBox("Usage", usageMsg);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Display edit description window
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::editDescription(){
    // If album is empty, return error message
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    // Create a new edit description dialog
    EditDescription editDescription(NULL, this);
    // Show dialog
    editDescription.setModal(true);
    editDescription.exec();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when crop image button is clicked
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::cropImage(){
    // If album is empty, return error message
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    // Say filename is the file path of current image being viewed
    QString fileName = album->photos.at(current_image_index).filename;
    // Create new cropping window
    croppingImageLabel = new CroppingImageLabel(fileName, this);
    // Show cropping window
    croppingImageLabel->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on brighten option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::brighten(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "brighten");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on contrast option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::contrast(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "contrast");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on edge option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::edge(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    edgeImage(album->photos.at(current_image_index).filename);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on gamma option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::gamma(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "gamma");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on negate option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::negate(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    negateImage(album->photos.at(current_image_index).filename);
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on sharpen option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::sharpen(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "sharpen");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on smooth option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::smooth(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "smooth");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on resize option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::resize(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SliderBarDialog *sliderBarDialog = new SliderBarDialog(NULL, this, "resize");
    sliderBarDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user clicks on rotate option
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::rotate(){
    if(album->photos.isEmpty()){
        createDialogBox("Information", "There is no image to edit");
        return;
    }
    SpinnerBoxDialog *spinnerBoxDialog = new SpinnerBoxDialog(NULL, this);
    spinnerBoxDialog->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Creates a dialog box with an arbitrary title and message
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::createDialogBox(QString title, QString message){
    QMessageBox::information(this, tr(title.toStdString().c_str()),
                             tr(message.toStdString().c_str()));
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Creates tooltips for buttons, and connects signals to slots
 *
 * @returns nothing
 *****************************************************************************/
void MainWindow::createActions()
{
    ui->actionAbout->setShortcut(tr("Ctrl+A"));
    ui->actionAbout->setStatusTip(tr("About"));
    QPixmap pixabout(":/new/prefix1/images/icons/help.png");
    QIcon iconabout(pixabout);
    ui->actionAbout->setIcon(iconabout);
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));

    ui->actionUsage->setShortcut(tr("Ctrl+U"));
    ui->actionUsage->setStatusTip(tr("Usage"));
    connect(ui->actionUsage, SIGNAL(triggered()), this, SLOT(usage()));

    ui->actionQuit->setShortcut(tr("Ctrl+Q"));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));

    ui->actionOpen->setShortcut(tr("Ctrl+O"));
    ui->actionOpen->setStatusTip(tr("Open album"));
    QPixmap pixopen(":/new/prefix1/images/icons/open.png");
    QIcon iconopen(pixopen);
    ui->actionOpen->setIcon(iconopen);
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()));

    ui->actionPage_forward->setShortcut(tr("Right"));
    ui->actionPage_forward->setStatusTip(tr("Move to next image..."));
    QPixmap pixforward(":/new/prefix1/images/icons/arrow-right.png");
    QIcon iconforward(pixforward);
    ui->actionPage_forward->setIcon(iconforward);
    connect(ui->actionPage_forward, SIGNAL(triggered()), this, SLOT(pageForward()));

    ui->actionPage_backward->setShortcut(tr("Left"));
    ui->actionPage_backward->setStatusTip(tr("Move to previous image..."));
    QPixmap pixbackward(":/new/prefix1/images/icons/arrow-left.png");
    QIcon iconbackward(pixbackward);
    ui->actionPage_backward->setIcon(iconbackward);
    connect(ui->actionPage_backward, SIGNAL(triggered()), this, SLOT(pageBackward()));

    ui->actionMove_forward->setShortcut(tr("Ctrl+Right"));
    ui->actionMove_forward->setStatusTip(tr("Swap with next image... "));
    QPixmap pixmforward(":/new/prefix1/images/icons/arrow-up.png");
    QIcon iconmforward(pixmforward);
    ui->actionMove_forward->setIcon(iconmforward);
    connect(ui->actionMove_forward, SIGNAL(triggered()), this, SLOT(moveForward()));

    ui->actionMove_backward->setShortcut(tr("Ctrl+Left"));
    ui->actionMove_backward->setStatusTip(tr("Swap with previous image..."));
    QPixmap pixmbackward(":/new/prefix1/images/icons/arrow-down.png");
    QIcon iconmbackward(pixmbackward);
    ui->actionMove_backward->setIcon(iconmbackward);
    connect(ui->actionMove_backward, SIGNAL(triggered()), this, SLOT(moveBackward()));

    ui->actionClose->setShortcut(tr("Ctrl+W"));
    ui->actionClose->setStatusTip(tr("Close Album"));
    QPixmap pixClose(":/new/prefix1/images/icons/delete.png");
    QIcon iconClose(pixClose);
    ui->actionClose->setIcon(iconClose);
    connect(ui->actionClose, SIGNAL(triggered()), this, SLOT(closeAlbum()));

    ui->actionNew->setShortcut(tr("Ctrl+N"));
    ui->actionNew->setStatusTip(tr("New album"));
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newAlbum()));

    ui->actionAdd_photo->setShortcut(tr("Ctrl+Ins"));
    ui->actionAdd_photo->setStatusTip(tr("Add photo"));
    QPixmap pixadd(":/new/prefix1/images/icons/plus-sign.png");
    QIcon iconadd(pixadd);
    ui->actionAdd_photo->setIcon(iconadd);
    connect(ui->actionAdd_photo, SIGNAL(triggered()), this, SLOT(addPhoto()));

    ui->actionDelete_photo->setShortcut(tr("Ctrl+Del"));
    ui->actionDelete_photo->setStatusTip(tr("Delete photo"));
    QPixmap pixdelete(":/new/prefix1/images/icons/minus-sign.png");
    QIcon icondelete(pixdelete);
    ui->actionDelete_photo->setIcon(icondelete);
    connect(ui->actionDelete_photo, SIGNAL(triggered()), this, SLOT(deletePhoto()), Qt::UniqueConnection);

    ui->actionSave->setShortcut(tr("Ctrl+S"));
    ui->actionSave->setStatusTip(tr("Save"));
    QPixmap pixsave(":/new/prefix1/images/icons/save.png");
    QIcon iconsave(pixsave);
    ui->actionSave->setIcon(iconsave);
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveAlbum()));

    ui->actionSave_As->setShortcut(tr("Ctrl+Shift+S"));
    ui->actionSave_As->setStatusTip(tr("Save As"));
    connect(ui->actionSave_As, SIGNAL(triggered()), this, SLOT(saveAsAlbum()));

    ui->actionEdit_description->setShortcut(tr("Ctrl+E"));
    ui->actionEdit_description->setStatusTip(tr("Edit Description"));
    QPixmap pixedit(":/new/prefix1/images/icons/edit.png");
    QIcon iconedit(pixedit);
    ui->actionEdit_description->setIcon(iconedit);
    connect(ui->actionEdit_description, SIGNAL(triggered()), this, SLOT(editDescription()));

    ui->actionCrop->setShortcut(tr(""));
    ui->actionCrop->setStatusTip(tr("Crop"));
    connect(ui->actionCrop, SIGNAL(triggered()), this, SLOT(cropImage()));

    ui->actionBrightness->setShortcut(tr(""));
    ui->actionBrightness->setStatusTip(tr("Brigthen Image"));
    connect(ui->actionBrightness, SIGNAL(triggered()), this, SLOT(brighten()));

    ui->actionContrast->setShortcut(tr(""));
    ui->actionContrast->setStatusTip(tr("Contrast Image"));
    connect(ui->actionContrast, SIGNAL(triggered()), this, SLOT(contrast()));

    ui->actionEdge->setShortcut(tr(""));
    ui->actionEdge->setStatusTip(tr("Edge Image"));
    connect(ui->actionEdge, SIGNAL(triggered()), this, SLOT(edge()));

    ui->actionGamma->setShortcut(tr(""));
    ui->actionGamma->setStatusTip(tr("Gamma Image"));
    connect(ui->actionGamma, SIGNAL(triggered()), this, SLOT(gamma()));

    ui->actionNegate->setShortcut(tr(""));
    ui->actionNegate->setStatusTip(tr("Negate Image"));
    connect(ui->actionNegate, SIGNAL(triggered()), this, SLOT(negate()));

    ui->actionSharpen->setShortcut(tr(""));
    ui->actionSharpen->setStatusTip(tr("Sharpen Image"));
    connect(ui->actionSharpen, SIGNAL(triggered()), this, SLOT(sharpen()));

    ui->actionSmooth->setShortcut(tr(""));
    ui->actionSmooth->setStatusTip(tr("Smooth Image"));
    connect(ui->actionSmooth, SIGNAL(triggered()), this, SLOT(smooth()));

    ui->actionResize->setShortcut(tr(""));
    ui->actionResize->setStatusTip(tr("Resize Image"));
    connect(ui->actionResize, SIGNAL(triggered()), this, SLOT(resize()));

    ui->actionRotate->setShortcut(tr(""));
    ui->actionRotate->setStatusTip(tr("Rotate Image"));
    connect(ui->actionRotate, SIGNAL(triggered()), this, SLOT(rotate()));
}
