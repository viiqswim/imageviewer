#include "editdescription.h"
#include "ui_editdescription.h"
#include "mainwindow.h"
#include <QtGui>


/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Constructor
 *
 *
 * @param[in] parent - Container widget
 * @param[in] *_mainWindow - pointer of main program main control
 *
 * @returns nothing
 *****************************************************************************/
EditDescription::EditDescription(QWidget *parent, MainWindow *_mainWindow) :
    QDialog(parent),
    ui(new Ui::EditDescription)
{
    ui->setupUi(this);
    setWindowTitle("Edit Description");
    mainWindow = _mainWindow;
    // Gets main control program album
    album = mainWindow->getAlbum();
    // Gets the index of current image being looked at
    current_image_index = mainWindow->getCurrentImageIndex();

    // Sets the image date field to existing data
    ui->dateEdit->setText(album.photos[current_image_index].date);
    // Sets the image description field to existing data
    ui->descriptionEdit->setText(album.photos[current_image_index].description);
    // Sets the image location field to existing data
    ui->locationEdit->setText(album.photos[current_image_index].location);
}


/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Deconstructor
 *
 * @returns nothing
 *****************************************************************************/
EditDescription::~EditDescription()
{
    delete ui;
}


/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when confirm button is clicked
 *
 * @returns nothing
 *****************************************************************************/
void EditDescription::on_confirmEditButton_clicked()
{
    // Sets the photo location to the value of text field
    mainWindow->setPhotoLocation(ui->locationEdit->text());
    // Sets the photo date to the value of text field
    mainWindow->setPhotoDate(ui->dateEdit->text());
    // Sets the photo description to the value of text field
    mainWindow->setPhotoDescription(ui->descriptionEdit->toPlainText());
    // Reopens photo with the new data
    mainWindow->openPhoto(current_image_index);
    // Closes the edit description dialog
    this->close();
}
