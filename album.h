#ifndef ALBUM_H
#define ALBUM_H

#include <QObject>
#include <QMainWindow>
#include <photo.h>


/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class  holds an album. Each album has a series of
 * photos stored in a QVector
 *
 *****************************************************************************/
class Album{
public:
    // Holds list of photos
    QVector<Photo> photos;
};

#endif // ALBUM_H

