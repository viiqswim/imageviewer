#include "sliderbardialog.h"
#include "ui_sliderbardialog.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Constructor
 *
 *
 * @param[in] parent - Container widget
 * @param[in] *_mainWindow - pointer of main program main control
 * @param[in] _operating - string defining what image processing operation to perform
 *
 * @returns nothing
 *****************************************************************************/
SliderBarDialog::SliderBarDialog(QWidget *parent, MainWindow *_mainWindow, QString _operation) :
    QDialog(parent),
    ui(new Ui::SliderBarDialog)
{
    ui->setupUi(this);
    setWindowTitle("Process Image");
    mainWindow = _mainWindow;
    operation = _operation;
    // Creates a default message to display every time
    QString default_msg = "\nKeep in mind that we are not responsible \nfor telling you the boundaries of an image \nprocessing operation.";
    // Depending on the operation specified by the user, set the window's text
    if(operation == "brighten"){
        ui->label->setText("Enter a number to modify image brigthness" + default_msg);
    } else if (operation == "contrast") {
        ui->label->setText("Enter a number to set the image's contrast" + default_msg);
    } else if (operation == "gamma") {
        ui->label->setText("Enter a number to modify image's gamma" + default_msg);
    } else if (operation == "sharpen") {
        ui->label->setText("Enter a number to sharpen image" + default_msg);
    } else if (operation == "smooth") {
        ui->label->setText("Enter a number to smooth image" + default_msg);
    } else if (operation == "resize") {
        ui->label->setText("Enter a number (in percentage) to resize image" + default_msg);
    }
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Deconstructor
 *
 * @returns nothing
 *****************************************************************************/
SliderBarDialog::~SliderBarDialog()
{
    delete ui;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when confirm button is clicked
 *
 * @returns nothing
 *****************************************************************************/
void SliderBarDialog::on_confirmButton_clicked()
{
    // Get the spinbox's value
    int value = ui->spinBox->value();
    // Depending on the operation specified by the user, modify the image
    if(operation == "brighten"){
        mainWindow->brightenImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    } else if (operation == "contrast") {
        mainWindow->contrastImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    } else if (operation == "gamma") {
        mainWindow->gammaImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    } else if (operation == "sharpen") {
        mainWindow->sharpenImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    } else if (operation == "smooth") {
        mainWindow->smoothImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    } else if (operation == "resize") {
        mainWindow->resizeImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    }

    // Closes the slider bar dialog
    this->close();
}
