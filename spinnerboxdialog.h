#ifndef SPINNERBOXDIALOG_H
#define SPINNERBOXDIALOG_H

#include <QDialog>
#include "mainwindow.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class is the window that shows the slider
 * and allows the user to pick a number between 0 and 360 to rotate an image
 *
 *****************************************************************************/
namespace Ui {
class SpinnerBoxDialog;
}

class SpinnerBoxDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SpinnerBoxDialog(QWidget *parent = 0, MainWindow *mainWindow = 0);
    ~SpinnerBoxDialog();

private slots:
    // Called when confirm button is clicked
    void on_confirmButton_clicked();

private:
    Ui::SpinnerBoxDialog *ui;
    // Pointer needed to call image processing functions
    MainWindow *mainWindow;
};

#endif // SPINNERBOXDIALOG_H
