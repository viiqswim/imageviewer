#ifndef PHOTO_H
#define PHOTO_H

#include <QObject>

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class contains information for any given photo,
 * including file name, date, location, and description
 *
 *****************************************************************************/
class Photo {
public:
    // String containing path to image, along with image name
    QString filename;
    // String containing date
    QString date;
    // String containing location
    QString location;
    // String containing description
    QString description;
};

#endif // PHOTO_H
