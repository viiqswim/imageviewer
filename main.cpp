/*************************************************************************//**
 * @file
 *
 * @brief Photo viewer and basic image processor
 *
 * @section course_section Graphical User Interfaces
 *
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @date October 14th 2014
 *
 * @par Professor:
 *         Dr. John Weiss
 *
 * @par Course:
 *         CSC 421/521
 *
 * @par Location:
 *         McLaury
 *
 * @section program_section Program Information
 *
 * @details
 * The age of chemical (film) photography lasted for almost two centuries.
 * During this period, printed photographs were saved in photo albums.
 * But over the past decade, film cameras have been supplanted by
 * digital cameras (often integrated into cell phones and tablets).
 * Digital photos are saved on digital media, and viewed on an electronic display.
 * Because digital “film” is so inexpensive, it is typical to blast away with digital
 * cameras, generating too many images for comfortable viewing. Digital slide shows tend
 * to be much more enjoyable when you incorporate only the better pictures into an
 * annotated digital photo album.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions (Linux-based system):
 *       1. Make sure you have qmake installed in your machine
 *       2. Run the following commands in succession:
 *              qmake imageviewer.pro
 *              make
 *       3. You should now have an executable file called imageviewer
 *       4. To run it, simply type ./imageviewer
 *
 * @section extra_features Extra Features
 * 1. The following image processing operations:
 *      a) Edge
 *      b) Gamma
 *      c) Negate
 *      d) Sharpen
 *      e) Smooth
 * 2. The ability to scroll through images forever (wrap around)
 * 3. At the bottom we show the total number of images the user has in the album,
 *      along with which image the user is currently looking at
 *
 *****************************************************************************/

#include "mainwindow.h"
#include <QApplication>

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Opens a new application and shows it
 *
 * @returns int - says whether program exits cleanly or not
 *****************************************************************************/
int main(int argc, char *argv[])
{
    // Creates new application with given arguments
    QApplication a(argc, argv);
    // Creates a new main window
    MainWindow w;
    // Shows main window
    w.show();

    // Executes application
    return a.exec();
}
