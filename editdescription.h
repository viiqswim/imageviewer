#ifndef EDITDESCRIPTION_H
#define EDITDESCRIPTION_H

#include <QDialog>
#include "mainwindow.h"
#include <album.h>

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class is the window that allows the user to specify
 * the description for an image, including date, location, and description.
 *
 *****************************************************************************/
namespace Ui {
class EditDescription;
}

class EditDescription : public QDialog
{
    Q_OBJECT

public:
    explicit EditDescription(QWidget *parent = 0, MainWindow *mainWindow = 0);
    ~EditDescription();

private slots:
    // Called when confirm button is clicked
    void on_confirmEditButton_clicked();

private:
    Ui::EditDescription *ui;
    // Holds vector of photos
    Album album;
    // Hodls pointer to be able to call functiosn from main control
    MainWindow *mainWindow;
    // Holds current image we are currently viewing
    int current_image_index;
};

#endif // EDITDESCRIPTION_H
