#include "croppingimagelabel.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Constructor
 *
 *
 * @param[in] fileName - path of file to crop
 * @param[in] *_mainWindow - pointer of main program main control
 *
 * @returns nothing
 *****************************************************************************/
CroppingImageLabel::CroppingImageLabel( QString fileName, MainWindow *_mainWindow ){
    // Retrieves image from file
    QImage image( fileName );
    // If image is null, there was an error
    if ( image.isNull() )
    {
        qDebug() << "Cannot load image file " << fileName;
        exit( -2 );
    }

    mainWindow = _mainWindow;
    // Set's the label's pixmap to the image's pixmap
    setPixmap( QPixmap::fromImage( image ) );
    // Changes the title of the window
    setWindowTitle( tr( "Image Cropper" ) );

    // Initializes the rubberband object
    rubberBand = new QRubberBand( QRubberBand::Rectangle, this );
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user presses mouse click
 *
 *
 * @param[in] event - holds information about the action that occurred
 *
 * @returns nothing
 *****************************************************************************/
void CroppingImageLabel::mousePressEvent( QMouseEvent *event ){
    // Gets origin of mouse click
    origin = event->pos();
    // Sets geometry of rubberband to rectangle
    rubberBand->setGeometry( QRect( origin, QSize() ) );
    // shows the rubberband
    rubberBand->show();
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user moves mouse
 *
 *
 * @param[in] event - holds information about the action that occurred
 *
 * @returns nothing
 *****************************************************************************/
void CroppingImageLabel::mouseMoveEvent( QMouseEvent *event ){
    // Modifies size of rubberband from origin, to the position of where mouse
    // curently is
    rubberBand->setGeometry( QRect( origin, event->pos() ).normalized() );
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user releases mouse click
 *
 *
 * @param[in] event - holds information about the action that occurred
 *
 * @returns nothing
 *****************************************************************************/
void CroppingImageLabel::mouseReleaseEvent( QMouseEvent *event ){
    // Hides rubberband
    rubberBand->hide();
    // gets the rectangle that the user selected
    QRect qRect = QRect( origin, event->pos() ).normalized();
    // Gets the pixmap that correlates rectangle selected, to the pixmap
    QPixmap pixMap = pixmap()->copy( qRect );
    // Sets label pixmap to the rectangle the user selected
    setPixmap(pixMap);

    // If user accepts to save image. then save it and display it
    if(mainWindow->saveImageInFile(pixMap)){
        mainWindow->openPhoto(mainWindow->getCurrentImageIndex());
    }

    // Close cropping image label
    this->close();

    return;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when user presses a key
 *
 *
 * @param[in] event - holds information about the action that occurred
 *
 * @returns nothing
 *****************************************************************************/
void CroppingImageLabel::keyPressEvent( QKeyEvent *event ){
    // If the user pressed escape, then exit program
    if ( event->key() == Qt::Key_Escape )
        exit( 0 );
}
