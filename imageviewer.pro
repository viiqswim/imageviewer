#-------------------------------------------------
#
# Project created by QtCreator 2014-10-04T19:55:23
#
#-------------------------------------------------

QT       += core gui
QT       += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = imageviewer
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    editdescription.cpp \
    croppingimagelabel.cpp \
    sliderbardialog.cpp \
    spinnerboxdialog.cpp

HEADERS  += mainwindow.h \
    photo.h \
    album.h \
    editdescription.h \
    croppingimagelabel.h \
    sliderbardialog.h \
    spinnerboxdialog.h

FORMS    += mainwindow.ui \
    editdescription.ui \
    spinnerboxdialog.ui \
    sliderbardialog.ui

RESOURCES += \
    Icons.qrc
