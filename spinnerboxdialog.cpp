#include "spinnerboxdialog.h"
#include "ui_spinnerboxdialog.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Constructor
 *
 *
 * @param[in] parent - Container widget
 * @param[in] *_mainWindow - pointer of main program main control
 *
 * @returns nothing
 *****************************************************************************/
SpinnerBoxDialog::SpinnerBoxDialog(QWidget *parent, MainWindow *_mainWindow) :
    QDialog(parent),
    ui(new Ui::SpinnerBoxDialog)
{
    ui->setupUi(this);
    setWindowTitle("Process Image");
    mainWindow = _mainWindow;
    // Sets label's text
    ui->label->setText("Move the slider to choose the number (in degrees) to rotate the image");
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Deconstructor
 *
 * @returns nothing
 *****************************************************************************/
SpinnerBoxDialog::~SpinnerBoxDialog()
{
    delete ui;
}

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: Called when confirm button is clicked
 *
 * @returns nothing
 *****************************************************************************/
void SpinnerBoxDialog::on_confirmButton_clicked()
{
    // When the confirm button is clicked, get the current value of the slider
    int value = ui->horizontalSlider->value();
    // Rotate image by the current value in the slider
    mainWindow->rotateImage(mainWindow->getAlbum().photos.at(mainWindow->getCurrentImageIndex()).filename, value);
    // Close spinner box dialog
    this->close();
}
