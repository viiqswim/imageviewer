#ifndef SLIDERBARDIALOG_H
#define SLIDERBARDIALOG_H

#include <QDialog>
#include "mainwindow.h"

/**************************************************************************//**
 * @author Victor Dozal, Subhakar Yarlagadda
 *
 * @par Description: This class is the window that shows the spin box
 * and allows the user to pick a number between 0 and 100 to do image processing
 *
 *****************************************************************************/
namespace Ui {
class SliderBarDialog;
}

class SliderBarDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SliderBarDialog(QWidget *parent = 0, MainWindow *mainWindow = 0, QString _operation = "");
    ~SliderBarDialog();

private slots:
    // Called when confirm button is clicked
    void on_confirmButton_clicked();

private:
    Ui::SliderBarDialog *ui;
    // Pointer needed to call image processing functions
    MainWindow *mainWindow;
    // Contains a string saying what image processing operation
    // the user wants to do
    QString operation;
};

#endif // SLIDERBARDIALOG_H
